//
//  AppDelegate.swift
//  TukTukTravel
//
//  Created by Rizabek on 06.08.2021.
//

import UIKit
import ReSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var router: Router?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    /// Setup UIWindow
    setupWindow()
    
    /// Setup MainScreen
    setupMainScreen()
    
    /// Setup AccesToken
    setupAccesToken()
    
    return true
  }
  
  // MARK: - Setup UIWindow
  
  private func setupWindow() {
    let navigationController = UINavigationController()
    let assembly = MainAssembly()
    router = MainRouter(navigation: navigationController, assembly: assembly)
    router?.initial()
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
  
  // MARK: - Setup MainScreen
  
  private func setupMainScreen() {
    let originCity = UserDefaults.originCityModel
    let destinationCity = UserDefaults.destinationCityModel
    let currentDate = Date().string(format: "yyyy-MM-dd")
    mainStore.dispatch(MainActions.didSelectDate(currentDate, ""))
    mainStore.dispatch(MainActions.didSetOriginCity(originCity))
    mainStore.dispatch(MainActions.didSetDestinationCity(destinationCity))
  }
  
  // MARK: - Setup AccesToken
  
  private func setupAccesToken() {
    AuthorizationService.shared.authorization() { result in
      switch result {
      case .success(let token):
        KeychainService.appToken = token
      case .failure(let error):
        NSLog(error.localizedDescription)
      }
    }
  }
}
