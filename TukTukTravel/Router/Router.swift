//
//  Router.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit
import ReSwift

protocol Router {
  func initial()
  func presentPassengersView()
  func presentCalendarView()
  func presentSearchView()
  func presentFlightOfferView()
  init(navigation: UINavigationController?, assembly: Assembly?)
}

class MainRouter: Router, StoreSubscriber {
  
  private var navigation: UINavigationController?
  private var assembly: Assembly?
  
  func initial() {
    if let navigation = navigation {
      guard let vc = assembly?.initialController() else { return }
      navigation.setViewControllers([vc], animated: true)
    }
  }
  
  func presentPassengersView() {
    if let navigation = navigation {
      guard let vc = assembly?.presentPassengersView() else { return }
      navigation.present(vc, animated: true)
    }
  }
  
  func presentCalendarView() {
    if let navigation = navigation {
      guard let vc = assembly?.presentCalendarView() else { return }
      navigation.present(vc, animated: true)
    }
  }
  
  func presentSearchView() {
    if let navigation = navigation {
      guard let vc = assembly?.presentSearchView() else { return }
      navigation.present(vc, animated: true)
    }
  }
  
  func presentFlightOfferView() {
    if let navigation = navigation {
      guard let vc = assembly?.presentFlightOfferView() else { return }
      navigation.pushViewController(vc, animated: true)
    }
  }
  
  func popToRoot() {
    if let navigation = navigation {
      navigation.popToRootViewController(animated: true)
    }
  }
  
  func pop() {
    if let navigation = navigation {
      navigation.popViewController(animated: true)
    }
  }
  
  func dismiss() {
    if let navigation = navigation {
      navigation.dismiss(animated: true)
    }
  }
  
  required init(navigation: UINavigationController?, assembly: Assembly?) {
    self.navigation = navigation
    self.assembly = assembly
    self.navigation?.setNavigationBarHidden(true, animated: false)
    routeStore.subscribe(self)
  }
  
  func newState(state: RouteState) {
    switch state.state {
    case .main:
      initial()
    case .passengersView:
      presentPassengersView()
    case .calendarView:
      presentCalendarView()
    case .searchView:
      presentSearchView()
    case .flightOfferView:
      presentFlightOfferView()
    case .pop:
      pop()
    case .popToRoot:
      popToRoot()
    case .dismiss:
      dismiss()
    }
  }
}
