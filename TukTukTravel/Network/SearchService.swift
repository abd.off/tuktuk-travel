//
//  SearchService.swift
//  TukTukTravel
//
//  Created by Rizabek on 15.08.2021.
//

import Foundation

struct SearchService {
  private init() {}
  static let shared = SearchService()
  private let session = URLSession.shared
  
  func searchCity(_ query: String, completion: @escaping (Result<SearchModel, Swift.Error>) -> Void) {
    
    var baseUrl = URLComponents(string: ServiceConstants.searchCityUrl)
    baseUrl?.queryItems = [
      URLQueryItem(name: "term", value: query),
      URLQueryItem(name: "types", value: "city"),
      URLQueryItem(name: "locale", value: "en")
    ]
    
    guard let resultUrl = baseUrl?.url else {
      completion(.failure(Error(message: "failed to generate URL")))
      return
    }
    
    var request = URLRequest(url: resultUrl)
    request.httpMethod = "GET"

    session.dataTask(with: request) { data, response, error in
      if let error = error {
        completion(.failure(error))
        return
      }
      
      guard let response = response as? HTTPURLResponse else {
        completion(.failure(Error(message: "no response")))
        return
      }
      
      guard let data = data else {
        completion(.failure(Error(message: "no data")))
        return
      }
      
      guard ServiceConstants.validCodes ~= response.statusCode else {
        completion(.failure(Error(message: "status code is \(response.statusCode)")))
        return
      }
      
      do {
        let object = try JSONDecoder().decode(SearchModel.self, from: data)
        DispatchQueue.main.async {
          completion(.success(object))
        }
      } catch let error {
        completion(.failure(error))
      }
    }.resume()
  }
}
