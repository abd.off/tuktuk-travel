//
//  ThunkMiddleware.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import Foundation
import ReSwift
import ReSwiftThunk
 
let searchCityMiddleware = Thunk<MainState> { dispatch, getState in
    guard
        let state = getState(),
        !state.searching.isEmpty
    else {
        return
    }
  SearchService.shared.searchCity(state.searching) { result in
    switch result {
    case .success(let data):
      DispatchQueue.main.async {
        dispatch (
          MainActions.updateSearchResult(data)
        )
      }
    case .failure(let error):
      NSLog(error.localizedDescription)
    }
  }
}

let flightOfferMiddleware = Thunk<MainState> { dispatch, getState in
    guard let state = getState() else { return }
    dispatch(MainActions.startAnimation)
    FlightOffersService.shared.searchFlightOffer() { result in
      switch result {
      case .success(let value):
        DispatchQueue.main.async {
          dispatch(MainActions.loadedFlightOffers(value))
          dispatch(MainActions.stopAnimation)
        }
      case .failure(let error):
        NSLog(error.localizedDescription)
      }
    }
}
