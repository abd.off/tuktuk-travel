//
//  MainActions.swift
//  TukTukTravel
//
//  Created by Rizabek on 06.08.2021.
//

import ReSwift

enum MainActions: Action {
  case adultCounterIncrease
  case adultCounterDecrease
  case childrenCounterIncrease
  case childrenCounterDecrease
  case unplaceChildrenCounterIncrease
  case unplaceChildrenCounterDecrease
  case startAnimation
  case stopAnimation
  case readySearch(_ state: SearchState)
  case didChangeFlightClass(index: Int)
  case didSetOriginCity(_ newValue: OriginCityModel)
  case didSetDestinationCity(_ newValue: DestinationCityModel)
  case searchCity(_ query: String)
  case didSelectDate(_ fromDate: String, _ toDate: String)
  case reverseCities(_ from: OriginCityModel, _ to: DestinationCityModel)
  case updateSearchResult(SearchModel)
  case loadedFlightOffers(FlightOffer)
}
