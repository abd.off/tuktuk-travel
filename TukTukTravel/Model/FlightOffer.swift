//
//  FlightOffer.swift
//  TukTukTravel
//
//  Created by Rizabek on 18.08.2021.
//


import Foundation

// MARK: - FlightOffer

struct FlightOffer: Equatable, Decodable {
  let meta: Meta
  let data: [FlightData]
}

// MARK: - Datum

struct FlightData: Equatable, Decodable {
  let id: String
  let instantTicketingRequired, nonHomogeneous, oneWay: Bool
  let lastTicketingDate: String
  let numberOfBookableSeats: Int
  let itineraries: [Itinerary]
  let price: DatumPrice
  let validatingAirlineCodes: [String]
}

// MARK: - Itinerary

struct Itinerary: Equatable, Decodable {
  let duration: String
  let segments: [Segment]
}

// MARK: - Segment

struct Segment: Equatable, Decodable {
  let departure, arrival: Arrival
  let carrierCode: String
  let number: String
  let aircraft: SegmentAircraft
  let operating: Operating
  let duration: String
  let id: String
  let numberOfStops: Int
  let blacklistedInEU: Bool
}

// MARK: - SegmentAircraft

struct SegmentAircraft: Equatable, Decodable {
  let code: String
}

// MARK: - Arrival

struct Arrival: Equatable, Decodable {
  let iataCode: String
  let at: String
  let terminal: String?
}

// MARK: - Operating

struct Operating: Equatable, Decodable {
  let carrierCode: String
}

// MARK: - DatumPrice

struct DatumPrice: Equatable, Decodable {
  let currency: String
  let total, base: String
  let grandTotal: String
}

// MARK: - Location

struct Location: Equatable, Decodable {
  let cityCode, countryCode: String
}

// MARK: - Meta

struct Meta: Equatable, Decodable {
  let count: Int
}
