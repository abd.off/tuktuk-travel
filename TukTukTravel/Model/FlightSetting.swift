//
//  FlightSetting.swift
//  TukTukTravel
//
//  Created by Rizabek on 18.08.2021.
//

import Foundation

struct FlightSetting {
  let originLocationCode: String
  let destinationLocationCode: String
  let departureDate: String
  let adults: Int
  let children: Int
  let infants: Int
  let travelClass: TravelClass
  let nonStop: Bool
  
}

enum TravelClass: String, Equatable {
  case economy = "ECONOMY"
  case business = "BUSINESS"
  case first = "FIRST"
}
