//
//  SearchModel.swift
//  TukTukTravel
//
//  Created by Rizabek on 15.08.2021.
//

import Foundation

// MARK: - SearchCity

struct SearchCity: Decodable, Equatable {
    let countryCode,
        name,
        mainAirportName,
        code: String?
    let countryName: String?

    enum CodingKeys: String, CodingKey {
        case countryCode = "country_code"
        case name
        case mainAirportName = "main_airport_name"
        case code
        case countryName = "country_name"
    }
}

typealias SearchModel = [SearchCity]
