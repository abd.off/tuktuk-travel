//
//  UITableView + Reload.swift
//  TukTukTravel
//
//  Created by Rizabek on 17.08.2021.
//

import UIKit

extension UITableView {
  func fadeReload() {
    UIView.animate(withDuration: 0.34) { [weak self] in
      guard let self = self else { return }
      self.alpha = 0
      self.reloadData()
    } completion: { [weak self] _ in
      guard let self = self else { return }
      UIView.animate(withDuration: 0.34) {
        self.alpha = 1
      }
    }
  }
  
  func reloadAnimation() {
    UIView.transition(with: self, duration: 0.34, options: .curveLinear) { [weak self] in
      guard let self = self else { return }
      self.reloadData()
    }
  }
}
