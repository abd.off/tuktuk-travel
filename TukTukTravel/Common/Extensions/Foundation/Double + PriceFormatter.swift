//
//  Double + PriceFormatter.swift
//  TukTukTravel
//
//  Created by Rizabek on 23.08.2021.
//

import Foundation

extension Double {
  
  func getFormattedPrice () -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = NumberFormatter.Style.decimal
    numberFormatter.string(from: NSNumber(value: self))
    let priceFormatted = numberFormatter.string(from: NSNumber(value: self)) ?? ""
    return priceFormatted
  }
}
