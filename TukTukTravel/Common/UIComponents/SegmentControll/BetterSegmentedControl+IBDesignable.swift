//
//  BetterSegmentedControl+IBDesignable.swift
//  BetterSegmentedControl
//
//  Created by George Marmaridis on 20.10.20.
//

import UIKit

extension BetterSegmentedControl {
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setDefaultColorsIfNeeded()
    }
    open override func awakeFromNib() {
        super.awakeFromNib()
        setDefaultColorsIfNeeded()
    }
    private func setDefaultColorsIfNeeded() {
      if backgroundColor == UIColor.systemBackground || backgroundColor == nil {
        backgroundColor = .black
      }
      if indicatorViewBackgroundColor == UIColor.systemBackground || indicatorViewBackgroundColor == nil {
        indicatorViewBackgroundColor = .black
      }
    }
}
