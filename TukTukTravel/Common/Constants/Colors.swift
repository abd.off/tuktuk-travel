//
//  Colors.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIColor

struct Colors {
  
  /// primary
  static let primary = UIColor(hex: "#733FE1") ?? .clear
  
  /// secondary
  static let secondary = UIColor(hex: "#733FE1", alpha: 0.6) ?? .clear
  
  /// secondary2
  static let secondary2 = UIColor(hex: "#E7E6F9") ?? .clear
  
  /// primary
  static let background = UIColor(hex: "#F1F1F2") ?? .clear

  /// gray
  static let gray = UIColor(hex: "#808080") ?? .clear
  
  /// gray
  static let gray2 = UIColor(hex: "#C4C4C4") ?? .clear
  
  /// gray
  static let lightGray = UIColor(hex: "#F2F2F7") ?? .clear
  
  /// red
  static let red = UIColor(hex: "#F40000") ?? .clear
}
