//
//  ImageLoader.swift
//  TukTukTravel
//
//  Created by Rizabek on 24.08.2021.
//

import UIKit

final class ImageLoader {
  
  fileprivate let cache = NSCache<NSString, UIImage>()
  
  private init() {}
  static let shared = ImageLoader()
  
  func loadImage(_ urlString: String, completionHandler: @escaping (_ image: UIImage?, _ error: Swift.Error?) -> ()) {
    
    DispatchQueue.global(qos: .background).async {
      if let cachedImage = self.cache.object(forKey: urlString as NSString) {
        DispatchQueue.main.async {
          completionHandler(cachedImage, nil)
        }
        return
      }
      
      let session = URLSession.shared
      guard let url = URL(string: urlString) else { return }
      
      let request = URLRequest(url: url)
      
      session.dataTask(with: request) { data, response, error in
        
        if let error = error {
          completionHandler(nil, error)
          return
        }
        
        if let data = data {
          if let image = UIImage(data: data) {
            self.cache.setObject(image, forKey: urlString as NSString)
            DispatchQueue.main.async {
              completionHandler(image, nil)
            }
          }
        }
      }.resume()
    }
  }
}
