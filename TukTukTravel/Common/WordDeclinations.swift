//
//  WordDeclinations.swift
//  TukTukTravel
//
//  Created by Rizabek on 15.08.2021.
//

import Foundation

public final class WordDeclinations {
  let singularNominative: String
  let genetiveSingular: String
  let genetivePlural: String
  
  public init(_ singularNominative: String, _ genetiveSingular: String, _ genetivePlural: String) {
    self.singularNominative = singularNominative
    self.genetiveSingular = genetiveSingular
    self.genetivePlural = genetivePlural
  }
}

public final class WordDeclinationSelector {
  
  public static func declineWord(for number: Int, from declensions: WordDeclinations) -> String {
    let ending = number % 100
    if (ending >= 11 && ending <= 19) {
      return declensions.genetivePlural
    }
    switch (ending % 10) {
    case 1:
      return declensions.singularNominative
    case 2, 3, 4:
      return declensions.genetiveSingular
    default:
      return declensions.genetivePlural
    }
  }
}
