//
//  Store.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import ReSwift
import ReSwiftThunk

let thunksMiddleware: Middleware<MainState> = createThunkMiddleware()
let mainStore = Store(
    reducer: appReducer,
    state: MainState(),
    middleware: [thunksMiddleware]
)

let routeStore = Store(
  reducer: routeReducer,
  state: RouteState(),
  middleware: []
)
