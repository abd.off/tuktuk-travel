//
//  SearchCityCell.swift
//  TukTukTravel
//
//  Created by Rizabek on 17.08.2021.
//

import UIKit

final class SearchCityCell: UITableViewCell {
  
  // MARK: - Properties
  
  var searchResult: SearchCity? {
    didSet {
      guard let searchResult = searchResult else { return }
      cityLabel.text = searchResult.name
      countryLabel.text = searchResult.countryName
      cityCodeLabel.text = searchResult.code
    }
  }
  
  // MARK: - UI Elements
  
  private lazy var cityLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    return label
  }()
  
  private lazy var countryLabel: UILabel = {
    let label = UILabel()
    label.textColor = .lightGray
    return label
  }()
    
  private lazy var cityCodeLabel: UILabel = {
    let label = UILabel()
    label.textColor = .lightGray
    return label
  }()

  private lazy var underlineView: UIView = {
    let view = UIView()
    view.backgroundColor = .separator
    return view
  }()
  
  // MARK: - Init
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    addSubViews()
    addActionsToViews()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    layout()
  }
  
  private func addSubViews() {
    [cityLabel,
     countryLabel,
     cityCodeLabel,
     underlineView].forEach { contentView.addSubview($0) }
    contentView.setNeedsUpdateConstraints()
  }
  
  private func addActionsToViews() {
    let selectGest = UITapGestureRecognizer(target: self, action: #selector(didSelectAction))
    contentView.addGestureRecognizer(selectGest)
  }
  
  // MARK: - Set Layout
  
  fileprivate func layout() {
    cityLabel.pin.top(16).left(16).right(40).sizeToFit(.width)
    countryLabel.pin.below(of: cityLabel, aligned: .left).marginTop(8).right(40).sizeToFit(.width)
    cityCodeLabel.pin.vCenter().right(20).width(50).sizeToFit(.width)
    underlineView.pin.bottom().left(20).right().height(0.5)
  }
  
  // MARK: - Actions
  
  @objc private func didSelectAction() {
    if mainStore.state.search == .origin {
      mainStore.dispatch(
        MainActions.didSetOriginCity(OriginCityModel(cityName: cityLabel.text ?? "",
                                                     cityCode: cityCodeLabel.text ?? ""))
      )
    } else {
      mainStore.dispatch(
        MainActions.didSetDestinationCity(DestinationCityModel(cityName: cityLabel.text ?? "",
                                                     cityCode: cityCodeLabel.text ?? ""))
      )
    }
    routeStore.dispatch(RoutingState.dismiss)
  }
}
