//
//  CallendarViewController.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit
import ReSwift

private enum CalendarSelection {
  case singleDay(Day)
  case dayRange(DayRange)
}

final class CallendarViewController: UIViewController, PanModalPresentable {
  
  // MARK: - Properties
  
  var panScrollable: UIScrollView? {
    return nil
  }
  
  var longFormHeight: PanModalHeight {
    return .maxHeight
  }
  
  var anchorModalToLongForm: Bool {
    return false
  }
  
  var isHapticFeedbackEnabled: Bool {
    return true
  }
  
  var showDragIndicator: Bool {
    return true
  }
  
  private lazy var fromDate: String = ""
  private lazy var toDate: String = ""
  
  private var calendarSelection: CalendarSelection?
  private lazy var calendar = Calendar.current
  private let currentDate = Date()
  
  private lazy var dayDateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.calendar = calendar
    dateFormatter.locale = calendar.locale
    dateFormatter.dateFormat = DateFormatter.dateFormat(
      fromTemplate: "EEEE, MMM d, yyyy",
      options: 0,
      locale: calendar.locale ?? Locale.current)
    return dateFormatter
  }()
  
  // MARK: - UI Elements
  
  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Дата поездки"
    label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
    label.numberOfLines = 0
    label.textColor = .black
    return label
  }()
  
  private lazy var calendarView: CalendarView = {
    let calendarView = CalendarView(initialContent: makeContent())
    calendarView.daySelectionHandler = { [weak self] day in
      guard let self = self else { return }
      switch self.calendarSelection {
      case .singleDay(let selectedDay):
        if day > selectedDay {
          self.fromDate = selectedDay.description
          self.toDate = day.description
          self.calendarSelection = .dayRange(selectedDay...day)
        } else {
          self.calendarSelection = .singleDay(day)
        }
      case .none, .dayRange:
        self.fromDate = day.description
        self.toDate = ""
        self.calendarSelection = .singleDay(day)
      }
      calendarView.setContent(self.makeContent())
    }
    calendarView.scroll(toDayContaining: currentDate, scrollPosition: .centered, animated: false)
    return calendarView
  }()
  
  private lazy var applyContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    return view
  }()
  
  private lazy var applyButton: UIButton = {
    let button = UIButton()
    button.setTitle("Применить", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    button.backgroundColor = Colors.primary
    button.cornerRadius = 8
    button.addTarget(self, action: #selector(didSelectDates), for: .touchUpInside)
    return button
  }()
  
  // MARK: - LIFE CIRCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubviews()
  }
    
  // MARK: - Set Layout
  
  override func updateViewConstraints() {
    titleLabel.pin.top(40).left(20).right(20).sizeToFit(.width)
    calendarView.pin.below(of: titleLabel).marginTop(20).left().right().bottom(80).marginHorizontal(20)
    applyContainerView.pin.bottom().left().right().height(80)
    applyButton.pin.top(10).left().right().marginHorizontal(20).height(44)
    super.updateViewConstraints()
  }
  
  fileprivate func addSubviews() {
    applyContainerView.addSubview(applyButton)
    [titleLabel, calendarView, applyContainerView].forEach { view.addSubview($0)}
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
  }
  
  fileprivate func makeContent() -> CalendarViewContent {
    let startDate = calendar.date(from: DateComponents(year: 2021, month: 01, day: 01))!
    let endDate = calendar.date(from: DateComponents(year: 2023, month: 12, day: 31))!

    let calendarSelection = self.calendarSelection
    let dateRanges: Set<ClosedRange<Date>>
    if
      case .dayRange(let dayRange) = calendarSelection,
      let lowerBound = calendar.date(from: dayRange.lowerBound.components),
      let upperBound = calendar.date(from: dayRange.upperBound.components)
    {
      dateRanges = [lowerBound...upperBound]
    } else {
      dateRanges = []
    }

    return CalendarViewContent(
      calendar: calendar,
      visibleDateRange: startDate...endDate,
      monthsLayout: .vertical(options: VerticalMonthsLayoutOptions(pinDaysOfWeekToTop: false,
                                                                   alwaysShowCompleteBoundaryMonths: false)))

      .withInterMonthSpacing(24)
      .withVerticalDayMargin(8)
      .withHorizontalDayMargin(8)
      .withDayItemModelProvider { [weak self] day in
        let textColor: UIColor
        textColor = .label
        
        let isSelectedStyle: Bool
        switch calendarSelection {
        case .singleDay(let selectedDay):
          isSelectedStyle = day == selectedDay
        case .dayRange(let selectedDayRange):
          isSelectedStyle = day == selectedDayRange.lowerBound || day == selectedDayRange.upperBound
        case .none:
          isSelectedStyle = false
        }

        let dayAccessibilityText: String?
        if let date = self?.calendar.date(from: day.components) {
          dayAccessibilityText = self?.dayDateFormatter.string(from: date)
        } else {
          dayAccessibilityText = nil
        }

        return CalendarItemModel<DayView>(
          invariantViewProperties: .init(textColor: textColor, isSelectedStyle: isSelectedStyle),
          viewModel: .init(dayText: "\(day.day)", dayAccessibilityText: dayAccessibilityText))
      }

      .withDayRangeItemModelProvider(for: dateRanges) { dayRangeLayoutContext in
        CalendarItemModel<DayRangeIndicatorView>(
          invariantViewProperties: .init(),
          viewModel: .init(
            framesOfDaysToHighlight: dayRangeLayoutContext.daysAndFrames.map { $0.frame }))
      }
  }
  
  // MARK: - ACTIONS
  
  @objc private func didSelectDates() {
    mainStore.dispatch(MainActions.didSelectDate(fromDate, toDate))
    routeStore.dispatch(RoutingState.dismiss)
  }
}
