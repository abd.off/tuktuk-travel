//
//  MainViewController.swift
//  TukTukTravel
//
//  Created by Rizabek on 06.08.2021.
//

import UIKit
import ReSwift
import PinLayout

final class MainViewController: UIViewController, StoreSubscriber {
    
  // MARK: - UI Elements
  
  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Авиа"
    label.font = UIFont.systemFont(ofSize: 34, weight: .bold)
    label.numberOfLines = 0
    label.textColor = .black
    return label
  }()
  
  private lazy var fromCityName: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var fromLocationIcon: UIImageView = {
    let image = UIImageView()
    image.image = Images.locationPin
    return image
  }()
  
  private lazy var fromCityContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.addStandartShadow()
    view.cornerRadius = 8
    return view
  }()
  
  private lazy var toCityName: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var toLocationIcon: UIImageView = {
    let image = UIImageView()
    image.image = Images.locationPin
    return image
  }()
  
  private lazy var toCityContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.addStandartShadow()
    view.cornerRadius = 8
    return view
  }()
  
  private lazy var calendarValueLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var calendarIcon: UIImageView = {
    let image = UIImageView()
    image.image = Images.calendarIcon
    return image
  }()
  
  private lazy var calendarContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.addStandartShadow()
    view.cornerRadius = 8
    return view
  }()
  
  private lazy var passengerValueLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var passengerIcon: UIImageView = {
    let image = UIImageView()
    image.image = Images.userIcon
    return image
  }()
  
  private lazy var passengerContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.addStandartShadow()
    view.cornerRadius = 8
    return view
  }()
  
  private lazy var findTicketButton: UIButton = {
    let button = UIButton()
    button.setTitle("Найти билеты", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    button.backgroundColor = Colors.primary
    button.cornerRadius = 8
    button.addTarget(self, action: #selector(didFindTickets), for: .touchUpInside)
    return button
  }()
  
  private lazy var reverseButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.reverseArrowIcon, for: .normal)
    button.addTarget(self, action: #selector(didSwapCities), for: .touchUpInside)
    return button
  }()
  
  // MARK: - LIFE CIRCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubviews()
    addActionsToViews()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    mainStore.subscribe(self)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    mainStore.unsubscribe(self)
  }
  
  // MARK: - Set Layout
  
  override func updateViewConstraints() {
    titleLabel.pin.top(120).left(20).right(20).sizeToFit(.width)
    
    fromCityContainerView.pin.below(of: titleLabel, aligned: .left).marginTop(20).right(20).height(50)
    fromLocationIcon.pin.size(16).vCenter().left(12)
    fromCityName.pin.after(of: fromLocationIcon).vCenter().marginLeft(8).right(12).sizeToFit(.width)
    
    toCityContainerView.pin.below(of: fromCityContainerView, aligned: .left).right(to: fromCityContainerView.edge.right).marginTop(8).height(of: fromCityContainerView)
    toLocationIcon.pin.size(16).vCenter().left(12)
    toCityName.pin.after(of: toLocationIcon).vCenter().marginLeft(8).right(12).sizeToFit(.width)
    
    reverseButton.pin.size(36).below(of: fromCityContainerView).marginTop(-14).right(24)
    
    calendarContainerView.pin.below(of: toCityContainerView, aligned: .left).right(to: fromCityContainerView.edge.right).marginTop(20).height(of: fromCityContainerView)
    calendarIcon.pin.size(16).vCenter().left(12)
    calendarValueLabel.pin.after(of: calendarIcon).vCenter().marginLeft(8).right(12).sizeToFit(.width)
      
    passengerContainerView.pin.below(of: calendarContainerView, aligned: .left).right(to: fromCityContainerView.edge.right).marginTop(8).height(of: fromCityContainerView)
    passengerIcon.pin.size(16).vCenter().left(12)
    passengerValueLabel.pin.after(of: passengerIcon).vCenter().marginLeft(8).right(12).sizeToFit(.width)
    
    findTicketButton.pin.below(of: passengerContainerView, aligned: .left).right(to: fromCityContainerView.edge.right).marginTop(30).height(44)
    super.updateViewConstraints()
  }
  
  // MARK: - SET UI
  
  fileprivate func addSubviews() {
    [fromLocationIcon,
    fromCityName].forEach { fromCityContainerView.addSubview($0)}
    
    [toLocationIcon,
    toCityName].forEach { toCityContainerView.addSubview($0)}
    
    [calendarIcon,
    calendarValueLabel].forEach { calendarContainerView.addSubview($0)}
    
    [passengerIcon,
    passengerValueLabel].forEach { passengerContainerView.addSubview($0)}
    
    [titleLabel,
     fromCityContainerView,
     toCityContainerView,
     reverseButton,
     calendarContainerView,
     passengerContainerView,
     findTicketButton].forEach { view.addSubview($0)}
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
  }
  
  // MARK: - SET ACTIONS
  
  fileprivate func addActionsToViews() {
    let selectFromCiteGest = UITapGestureRecognizer(target: self, action: #selector(didSelectFromCity))
    fromCityContainerView.addGestureRecognizer(selectFromCiteGest)
    
    let selectToCityGest = UITapGestureRecognizer(target: self, action: #selector(didSelectToCity))
    toCityContainerView.addGestureRecognizer(selectToCityGest)
    
    let selectCalendarGest = UITapGestureRecognizer(target: self, action: #selector(didSelectCalendar))
    calendarContainerView.addGestureRecognizer(selectCalendarGest)
    
    let selectPassengerGest = UITapGestureRecognizer(target: self, action: #selector(didSelectPassenger))
    passengerContainerView.addGestureRecognizer(selectPassengerGest)
  }
  
  // MARK: - ACTIONS
  
  @objc private func didSelectFromCity() {
    mainStore.dispatch(MainActions.readySearch(.origin))
    routeStore.dispatch(RoutingState.searchView)
  }
  
  @objc private func didSelectToCity() {
    mainStore.dispatch(MainActions.readySearch(.destination))
    routeStore.dispatch(RoutingState.searchView)
  }
  
  @objc private func didSelectCalendar() {
    routeStore.dispatch(RoutingState.calendarView)
  }
  
  @objc private func didSelectPassenger() {
    routeStore.dispatch(RoutingState.passengersView)
  }
  
  @objc private func didSwapCities() {
    mainStore.dispatch(MainActions.reverseCities(OriginCityModel(cityName: mainStore.state.originCityModel?.cityName ?? "",
                                                                 cityCode: mainStore.state.originCityModel?.cityCode ?? ""),
                                                 DestinationCityModel(cityName:
                                                                        mainStore.state.destinationCityModel?.cityName ?? "",
                                                                      cityCode:
                                                                        mainStore.state.destinationCityModel?.cityCode ?? "")))
    
  }
  
  @objc private func didFindTickets() {
    routeStore.dispatch(RoutingState.flightOfferView)
  }
  
  // MARK: - StoreSubscriber
  
  func newState(state: MainState) {
    let totalPassengersCount = state.adultCount + state.infantsCount + state.childrenCount
    passengerValueLabel.text = "\(totalPassengersCount) пассажир, \(state.travelClass.rawValue.lowercased())"
    
    let toDate = state.returnDate.convertDate()
    let toDateValue = state.returnDate.isEmpty ? "" : "- \(toDate)"
    let fromDateValue = state.departureDate.convertDate()
    calendarValueLabel.text = "\(fromDateValue)" + " \(toDateValue)"
    
    fromCityName.text = "\(state.originCityModel?.cityName ?? "") "
    toCityName.text = "\(state.destinationCityModel?.cityName ?? "") "
  }
}
